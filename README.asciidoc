# Tests


./qovery container env list \
      --organization ${QOVERY_ORGANIZATION} \
      --project ${QOVERY_PROJECT} \
      --environment ${CURRENT_QOVERY_ENVIRONMENT} \
      --container ${APPLICATION_NAME} \
      --show-values true

./qovery container env delete \
      --key "GREETINGS_AGAIN" \
      --organization ${QOVERY_ORGANIZATION} \
      --project ${QOVERY_PROJECT} \
      --environment ${CURRENT_QOVERY_ENVIRONMENT} \
      --container ${APPLICATION_NAME}

./qovery container env create \
      --key "GREETINGS_AGAIN" \
      --value "Bill Balantine" \
      --scope "ENVIRONMENT" \
      --organization ${QOVERY_ORGANIZATION} \
      --project ${QOVERY_PROJECT} \
      --environment ${CURRENT_QOVERY_ENVIRONMENT} \
      --container ${APPLICATION_NAME}

function set_env() {
      echo "🟠 remove $1"
      ./qovery container env delete \
            --key $1 \
            --organization ${QOVERY_ORGANIZATION} \
            --project ${QOVERY_PROJECT} \
            --environment ${CURRENT_QOVERY_ENVIRONMENT} \
            --container ${APPLICATION_NAME}

      echo "🟣 create $1 $2"
      ./qovery container env create \
            --key "$1" \
            --value "$2" \
            --scope "ENVIRONMENT" \
            --organization ${QOVERY_ORGANIZATION} \
            --project ${QOVERY_PROJECT} \
            --environment ${CURRENT_QOVERY_ENVIRONMENT} \
            --container ${APPLICATION_NAME}
}

DOCKER_IMAGE_TAG="d3aa7808"
