#!/bin/bash
set -o allexport; source tmp.env; set +o allexport
echo "$QOVERY_CLI_ACCESS_TOKEN"
echo "$QOVERY_PROJECT"
echo "$CURRENT_QOVERY_ENVIRONMENT"
echo "$APPLICATION_NAME"

./qovery version

#set_env "GREETINGS_AGAIN" "🚀 rocket"

./qovery container env list \
      --organization ${QOVERY_ORGANIZATION} \
      --project ${QOVERY_PROJECT} \
      --environment ${CURRENT_QOVERY_ENVIRONMENT} \
      --container ${APPLICATION_NAME} \
      --show-values true

export QOVERY_CLI_ACCESS_TOKEN="qov_69m4zkqYr6OX9v8nZRW3e5YXHhFfmaB5z4lUocdJF8K2AF7pykzZB1g_792370737"

./qovery container env list \
      --organization "QoveryAivenDemo" \
      --project "prj-demo" \
      --environment "env-demo" \
      --container "env-demo" \
      --show-values true

./qovery container env delete \
      --key "GREETINGS_AGAIN" \
      --organization ${QOVERY_ORGANIZATION} \
      --project ${QOVERY_PROJECT} \
      --environment ${CURRENT_QOVERY_ENVIRONMENT} \
      --container ${APPLICATION_NAME}

./qovery container env create \
      --key "GREETINGS_AGAIN" \
      --value "Bill Balantine ..." \
      --scope "ENVIRONMENT" \
      --organization ${QOVERY_ORGANIZATION} \
      --project ${QOVERY_PROJECT} \
      --environment ${CURRENT_QOVERY_ENVIRONMENT} \
      --container ${APPLICATION_NAME}

