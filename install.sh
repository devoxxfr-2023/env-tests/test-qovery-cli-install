#!/bin/bash

QOVERY_CLI_VERSION="0.56.3"
sudo apt-get update
sudo apt-get install wget -y
wget https://github.com/Qovery/qovery-cli/releases/download/v${QOVERY_CLI_VERSION}/qovery-cli_${QOVERY_CLI_VERSION}_linux_amd64.tar.gz
tar xvf qovery-cli_${QOVERY_CLI_VERSION}_linux_amd64.tar.gz

./qovery version

